# WIR-Menus

WIR-Menus is the home for all of the menus located on [WIR](https://washingtonindianarestaurants.com)

## Contributing
Feel free to add a menu and I will add it to the site  
Create a [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)  
In your fork create the new menu based on the template  
Send a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
